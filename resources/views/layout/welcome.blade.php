<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Welcome Bootstrap</title>
        <!--Import bootstrap-->
        {!! Html::style('assets/css/bootstrap.min.css') !!}
                <style>
                    .navbar {
                        margin-bottom: 0;
                    }
                    .navbar-brand{padding: 8px 8px;}
                    h2{
                        margin: 0;
                        font-size: 40px;
                    }
                </style>
        <!-- librer�as opcionales que activan el soporte de HTML5 para IE8 -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar">1</span>
                    <span class="icon-bar">2</span>
                    <span class="icon-bar">3</span>
                </button>
                <a class="navbar-brand" href="#"><h2>B</h2></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">Iniciar sesi�n<span class="sr-only">(current)</span></a>
                    </li>
                    <li>
                        <a href="#">Registrate</a>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Buscar">
                    </div>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                </form>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
    <div class="jumbotron">
        <div class="container">
            <h1>Welcome Bootstrap</h1>
            <p>...</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
        </div>
    </div>

        <!--Import jQuery before bootstrap.js-->
        {!! Html::script('assets/js/jquery-2.1.1.min.js') !!}
        <!-- Scripts -->
        {!! Html::script('assets/js/bootstrap.min.js') !!}
        {!! Html::script('assets/js/holder.min.js') !!}

    </body>
</html>
